/**
 * Raytracer Code for Computer Graphics course 2012
 * Sander Feringa (1606913) and Jelmer van der Linde (1772791)
 */
//
//  Framework for a raytracer
//  File: material.cpp
//
//  Created for the Computer Science course "Introduction Computer Graphics"
//  taught at the University of Groningen by Tobias Isenberg.
//
//  Author: Maarten Everts
//
//  This framework is inspired by and uses code of the raytracer framework of 
//  Bert Freudenberg that can be found at
//  http://isgwww.cs.uni-magdeburg.de/graphik/lehre/cg2/projekt/rtprojekt.html 
//

#include "material.h"
#include "image.h"

#include <string>

using namespace std;

Material::Material()
:
    texture(0)
{}

Material::~Material()
{
	if (texture)
		delete texture;
}

void Material::loadTexture(string const &filename)
{
    if (texture)
        delete texture;

    texture = new Image(filename.c_str());
}

ostream& operator<<(ostream& out, Material const &m) 
{
    return out << "[Material color:" << m.color << " ka:" << m.ka << " kd:" << m.kd << " ks:" << m.ks << " n:" << m.n << "]"; 
}