/**
 * Raytracer Code for Computer Graphics course 2012
 * Sander Feringa (1606913) and Jelmer van der Linde (1772791)
 */

#include "plane.h"
#include <algorithm>
#include <iostream>
#include <math.h>

Hit Plane::intersect(const Ray &ray)
{
	// See whether the plane does not run exactly parallel to the ray
	if (N.dot(ray.D) == 0.0)
		return Hit::NO_HIT();

    double t = (ray.O - d).dot(N) / ray.D.dot(N);

    // See whether the plane is not behind the eye
    if (t < 0.0)
    	return Hit::NO_HIT();

    return Hit(t, N.normalized(), this);
}
