#ifndef CAMERA_H
#define CAMERA_H

#include "triple.h"

class Camera
{
public:
	Point eye; // E
	Point center; // M
	Vector up; // U
	int width; // 
	int height; // 
	double apertureRadius;
	unsigned int apertureSamples;

public:
	Camera() :
		apertureRadius(1.0),
		apertureSamples(1) {}
	
	Vector direction() {
		return center - eye;
	}

	Vector right() {
		return direction().normalized().cross(up);
	}
};

#endif