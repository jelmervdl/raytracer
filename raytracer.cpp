/**
 * Raytracer Code for Computer Graphics course 2012
 * Sander Feringa (1606913) and Jelmer van der Linde (1772791)
 */
//
//  Framework for a raytracer
//  File: raytracer.cpp
//
//  Created for the Computer Science course "Introduction Computer Graphics"
//  taught at the University of Groningen by Tobias Isenberg.
//
//  Author: Maarten Everts
//
//  This framework is inspired by and uses code of the raytracer framework of 
//  Bert Freudenberg that can be found at
//  http://isgwww.cs.uni-magdeburg.de/graphik/lehre/cg2/projekt/rtprojekt.html 
//

#include "raytracer.h"
#include "object.h"
#include "sphere.h"
#include "plane.h"
#include "triangle.h"
#include "cylinder.h"
#include "cube.h"
#include "model.h"
#include "menger.h"
#include "torus.h"
#include "material.h"
#include "light.h"
#include "image.h"
#include "camera.h"
#include "yaml/yaml.h"
#include <ctype.h>
#include <fstream>
#include <assert.h>
#include <stdexcept>

// Functions to ease reading from YAML input
void operator >> (const YAML::Node& node, Triple& t);
Triple parseTriple(const YAML::Node& node);

void operator >> (const YAML::Node& node, Triple& t)
{
    assert(node.size()==3);
    node[0] >> t.x;
    node[1] >> t.y;
    node[2] >> t.z;
}

Triple parseTriple(const YAML::Node& node)
{
    Triple t;
    node[0] >> t.x;
    node[1] >> t.y;
    node[2] >> t.z;	
    return t;
}

Scene::RenderMode Raytracer::parseRenderMode(const YAML::Node& node)
{
    std::string modeName;
    node >> modeName;

    if (modeName == "zbuffer")
        return Scene::ZBUFFER;

    else if (modeName == "normal")
        return Scene::NORMAL;

    else if (modeName == "gooch")
        return Scene::GOOCH;

    //if (modeName == "phong")
    else
        return Scene::PHONG;
}

Scene::GoochParameters Raytracer::parseGoochParameters(const YAML::Node& node)
{
    Scene::GoochParameters parameters;
    node["b"] >> parameters.b;
    node["y"] >> parameters.y;
    node["alpha"] >> parameters.alpha;
    node["beta"] >> parameters.beta;

    return parameters;
}

Material* Raytracer::parseMaterial(const YAML::Node& node)
{
    Material *m = new Material();
    
    if (node.FindValue("color"))
        node["color"] >> m->color;

    if (node.FindValue("texture"))
    {
        string filename;
        node["texture"] >> filename;
        m->loadTexture(filename);
    }

    node["ka"] >> m->ka;
    node["kd"] >> m->kd;
    node["ks"] >> m->ks;
    node["n"] >> m->n;
    return m;
}

Object* Raytracer::parseObject(const YAML::Node& node)
{
    Object *returnObject = 0;
    Material *material = 0;

    if (node.FindValue("material"))
        material = parseMaterial(node["material"]);

    std::string objectType;
    node["type"] >> objectType;

    if (objectType == "sphere") {
        Point pos;
        double angle, r;
        Vector axis(0, 1, 0);
        
        node["position"] >> pos;

        if (node.FindValue("angle"))
            node["angle"] >> angle;

        if (node["radius"].GetType() == YAML::CT_SCALAR)
            node["radius"] >> r;
        else
        {
            node["radius"][0] >> r;
            axis = parseTriple(node["radius"][1]);
        }

        Sphere *sphere = new Sphere(pos, r, axis, angle);
        sphere->material = material;
        returnObject = sphere;
    }

    else if (objectType == "plane") {
        Vector normal;
        node["normal"] >> normal;

        double distance;
        node["distance"] >> distance;

        Plane *plane = new Plane(normal, distance);
        plane->material = material;
        returnObject = plane;
    }
    
    else if (objectType == "torus") {
        Point pos;
        double r, R;
        
        node["position"] >> pos;

        if (node["r"].GetType() == YAML::CT_SCALAR)
            node["r"] >> r;
        else
        {
            node["r"][0] >> r;
            //up = parseTriple(node["r"][1]);
        }
        
        if (node["R"].GetType() == YAML::CT_SCALAR)
            node["R"] >> R;
        else
        {
            node["R"][0] >> R;
            //up = parseTriple(node["R"][1]);
        }

        Torus *torus = new Torus(pos, r, R);
        torus->material = material;
        returnObject = torus;
    }
    
    else if (objectType == "triangle") {
        Point p1, p2, p3;
        node["p1"] >> p1;
        node["p2"] >> p2;
        node["p3"] >> p3;

        Triangle *triangle = new Triangle(p1, p2, p3);
        triangle->material = material;
        returnObject = triangle;
    }

    else if (objectType == "cylinder") {
        Point p1, p2;
        node["p1"] >> p1;
        node["p2"] >> p2;

        double r;
        node["radius"] >> r;

        Cylinder *cylinder = new Cylinder(p1, p2, r);
        cylinder->material = material;
        returnObject = cylinder;
    }

    else if (objectType == "cube") {
        Cube *cube = new Cube(
            parseTriple(node["position"]),
            parseTriple(node["size"]),
            parseTriple(node["axis"]),
            node["angle"]);
        cube->material = material;
        returnObject = cube;
    }

    else if (objectType == "menger") {
        Menger *cube = new Menger(
            parseTriple(node["position"]),
            parseTriple(node["size"]),
            node["depth"],
            parseTriple(node["axis"]),
            node["angle"]);
        cube->material = material;
        returnObject = cube;
    }

    else if (objectType == "model") {
        Point position;
        node["position"] >> position;

        string filename;
        node["model"] >> filename;

        double scale;
        node["scale"] >> scale;

        Vector axis(0,1,0);
        if (node.FindValue("axis"))
            node["axis"] >> axis;

        double angle;
        if (node.FindValue("angle"))
            node["angle"] >> angle;

        Model *model = new Model(filename, position, axis, angle, scale, material);
        returnObject = model;
    }

    return returnObject;
}

Light* Raytracer::parseLight(const YAML::Node& node)
{
    Point position;
    node["position"] >> position;
    Color color;
    node["color"] >> color;
    return new Light(position,color);
}

Camera Raytracer::parseCamera(const YAML::Node& node)
{
    Camera camera;

    camera.eye = parseTriple(node["eye"]);
    camera.center = parseTriple(node["center"]);
    camera.up = parseTriple(node["up"]);
    camera.width = node["viewSize"][0];
    camera.height = node["viewSize"][1];

    if (node.FindValue("apertureRadius") != 0)
        camera.apertureRadius = node["apertureRadius"];

    if (node.FindValue("apertureSamples") != 0)
        camera.apertureSamples = node["apertureSamples"];

    return camera;
}

/*
* Read a scene from file
*/
bool Raytracer::readScene(const std::string& inputFilename)
{
    // Initialize a new scene
    scene = new Scene();

    // Open file stream for reading and have the YAML module parse it
    std::ifstream fin(inputFilename.c_str());
    if (!fin) {
        cerr << "Error: unable to open " << inputFilename << " for reading." << endl;;
        return false;
    }
    try {
        YAML::Parser parser(fin);
        if (parser) {
            YAML::Node doc;
            parser.GetNextDocument(doc);

            // Read scene configuration options
            

            if (doc.FindValue("RenderMode") != 0)
                scene->setRenderMode(parseRenderMode(doc["RenderMode"]));

            if (doc.FindValue("GoochParameters"))
                scene->setGoochParameters(parseGoochParameters(doc["GoochParameters"]));

            if (doc.FindValue("Shadows") != 0)
                scene->setRenderShadows(doc["Shadows"]);

            if (doc.FindValue("MaxRecursionDepth") != 0)
                scene->setMaxRecursionDepth(doc["MaxRecursionDepth"]);

            if (doc.FindValue("SuperSampling") != 0)
            {
                const YAML::Node &superSamplingSettings = doc["SuperSampling"];

                if (superSamplingSettings.FindValue("factor") != 0)
                    scene->setSuperSamplingFactor(superSamplingSettings["factor"]);
            }

            if (doc.FindValue("Camera") != 0)
                scene->setCamera(parseCamera(doc["Camera"]));
            else if (doc.FindValue("Eye") != 0)
                scene->setEye(parseTriple(doc["Eye"]));
            else
                throw std::runtime_error("No camera nor eye settings defined");

            // Read and parse the scene objects
            const YAML::Node& sceneObjects = doc["Objects"];
            if (sceneObjects.GetType() != YAML::CT_SEQUENCE) {
                cerr << "Error: expected a sequence of objects." << endl;
                return false;
            }
            for(YAML::Iterator it=sceneObjects.begin();it!=sceneObjects.end();++it) {
                Object *obj = parseObject(*it);
                // Only add object if it is recognized
                if (obj) {
                    scene->addObject(obj);
                } else {
                    cerr << "Warning: found object of unknown type, ignored." << endl;
                }
            }

            // Read and parse light definitions
            const YAML::Node& sceneLights = doc["Lights"];
            if (sceneObjects.GetType() != YAML::CT_SEQUENCE) {
                cerr << "Error: expected a sequence of lights." << endl;
                return false;
            }
            for(YAML::Iterator it=sceneLights.begin();it!=sceneLights.end();++it) {
                scene->addLight(parseLight(*it));
            }
        }
        if (parser) {
            cerr << "Warning: unexpected YAML document, ignored." << endl;
        }
    } catch(YAML::ParserException& e) {
        std::cerr << "Error at line " << e.mark.line + 1 << ", col " << e.mark.column + 1 << ": " << e.msg << std::endl;
        return false;
    }

    cout << "YAML parsing results: " << scene->getNumObjects() << " objects read." << endl;
    return true;
}

void Raytracer::renderToFile(const std::string& outputFilename)
{
    Image img(scene->getCamera().width, scene->getCamera().height);

    cout << "Tracing..." << endl;
    scene->render(img);
    cout << "Writing image to " << outputFilename << "..." << endl;
    img.write_png(outputFilename.c_str());
    cout << "Done." << endl;
}
