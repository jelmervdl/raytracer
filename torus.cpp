/**
 * Raytracer Code for Computer Graphics course 2012
 * Sander Feringa (1606913) and Jelmer van der Linde (1772791)
 * Code for the torus is based on:
 * http://code.google.com/p/raytrace-nacl/source/browse/trunk/torus.cc?r=9
 * http://code.google.com/p/raytrace-nacl/source/browse/trunk/solver.cc?r=9
 */
 
#include "torus.h"
#include <algorithm>
#include <iostream>
#include <math.h>
#include <limits>
#include <cassert>
#include <vector>

// Floating points arithmetic isn't science, it is stupid.
static const double ALMOST_ZERO = 1.0e-10;

Torus::Torus(Point position, double r, double R)
:
    position(position),
    r(r),
    R(R)
{}

void SolveCubic(const vector<double>& coeffs, vector<double>& solutions)
{
    // Normal form: x^3 + Ax^2 + Bx + C = 0
    const double A = coeffs[1] / coeffs[0];
    const double B = coeffs[2] / coeffs[0];
    const double C = coeffs[3] / coeffs[0];
  
    // Substitute x = y - A/3 to eliminate quadric term: x^3 + px + q = 0.
  
    const double sq_A = A * A;
    const double p = 1.0f / 3 * (-1.0f / 3 * sq_A + B);
    const double q = 1.0f / 2 * (2.0f / 27 * A * sq_A - 1.0f / 3 * A * B + C);
  
    // use Cardano's formula
    const double cb_p = p * p * p;
    const double det = q * q + cb_p;
  
    if (fabs(det <= ALMOST_ZERO))
    {
        if (fabs(q <= ALMOST_ZERO))
            solutions.push_back(0);
        else
        {
            const double u = cbrt(-q);
            solutions.push_back(2 * u);
            solutions.push_back(-u);
        }
    }
    else if (det < 0)
    {
        const double phi = 1.0f/3 * acos(-q / sqrt(- cb_p));
        const double t = 2 * sqrt(-p);
  
        solutions.push_back(t * cos(phi));
        solutions.push_back(t * cos(phi + M_PI / 3));
        solutions.push_back(t * cos(phi - M_PI / 3));
    }
    else
    {
        const double sqrt_det = sqrt(det);
        const double u = cbrt(sqrt_det - q);
        const double v = -cbrt(sqrt_det + q);
  
        solutions.push_back(u + v);
    }
  
    // Resubstitute
    const double sub = 1.0 / 3 * A;
  
    for (vector<double>::iterator it = solutions.begin();
        it != solutions.end();
        ++it)
      *it = *it - sub;
}

void SolveQuadric(const vector<double>& coeffs, std::vector<double>& solutions)
{
    // Normal form: x^2 + px + q = 0
    const double p = coeffs[1] / (2 * coeffs[0]);
    const double q = coeffs[2] / coeffs[0];

    const double det = p * p - q;

    if (fabs(det <= ALMOST_ZERO))
      solutions.push_back(-p);
    
    else if (det > 0)
    {
        const double sqrt_det = sqrt(det);
        
        solutions.push_back(sqrt_det - p);
        solutions.push_back(-sqrt_det - p);
    }
}

void Solve(const vector<double>& coeffs, vector<double>& solutions)
{
    // Normal form: x^4 + Ax^3 + Bx^2 + Cx + D = 0
    const double A = coeffs[1] / coeffs[0];
    const double B = coeffs[2] / coeffs[0];
    const double C = coeffs[3] / coeffs[0];
    const double D = coeffs[4] / coeffs[0];
  
    // Substitute x = y - A/4 to eliminate cubic term: x^4 + px^2 + qx + r = 0.
  
    const double sq_A = A * A;
  
    const double p = -3.0 / 8 * sq_A + B;
    const double q =  1.0 / 8 * sq_A * A - 1.0 / 2 * A * B + C;
    const double r = -3.0 / 256 * sq_A * sq_A + 1.0 / 16 * sq_A * B - 1.0 / 4 * A * C + D;
  
    if (fabs(r <= ALMOST_ZERO))
    {
        // No absolute term: y(y^3 + py + q) = 0
        vector<double> cubic_c;
        cubic_c.push_back(1);
        cubic_c.push_back(0);
        cubic_c.push_back(p);
        cubic_c.push_back(q);

        SolveCubic(cubic_c, solutions);

        solutions.push_back(0);
    }
    else
    {
        // Solve the resolvent cubic: y^3 - (p/2)x^2 - r.x + r.p/2 - (q^2)/8.
        vector<double> cubic_c;
        cubic_c.push_back(1);
        cubic_c.push_back(-p / 2.0f);
        cubic_c.push_back(-r);
        cubic_c.push_back((r * p / 2.0f) - (q * q / 8.0f));
    
        vector<double> cubic_s;
        SolveCubic(cubic_c, cubic_s);
    
        // And take the one real solution to build two quadric equations.
        const double z = cubic_s[0];
    
        double u = z * z - r;
        double v = 2 * z - p;
    
        if (fabs(u <= ALMOST_ZERO))
            u = 0;
        else if (u > 0)
            u = sqrt(u);
        else
          return;
        

        if (fabs(v <= ALMOST_ZERO))
          v = 0;
        else if (v > 0)
          v = sqrt(v);
        else
          return;
    
        vector<double> quadric_c;
        quadric_c.push_back(1);
        quadric_c.push_back(q < 0 ? -v : v);
        quadric_c.push_back(z - u);
    
        SolveQuadric(quadric_c, solutions);
    
        quadric_c.clear();
        quadric_c.push_back(1);
        quadric_c.push_back(q < 0 ? v : -v);
        quadric_c.push_back(z + u);
    
        SolveQuadric(quadric_c, solutions);
    }
}


Hit Torus::intersect(const Ray &ray)
{     
    // Translate the ray's position to object space
    Point local_ray_pos = ray.O - position;

    double a = ray.D.dot(ray.D);
    double b = 2.0 * local_ray_pos.dot(ray.D);
    double c = local_ray_pos.dot(local_ray_pos) - r*r - R*R;
    
    vector<double> coeffs;
  	coeffs.push_back(a * a);
  	coeffs.push_back(2.0 * a * b);
  	coeffs.push_back((b * b) + (2.0 * a * c) + (4.0 * R * R * ray.D.y * ray.D.y));
  	coeffs.push_back((2.0 * b * c) + (8.0 * R * R * local_ray_pos.y * ray.D.y));
  	coeffs.push_back((c * c) + (4.0 * R * R * local_ray_pos.y * local_ray_pos.y) - (4.0 * R * R * r * r));

    vector<double> solutions;
  	Solve(coeffs, solutions);

    // nah, this never happens
    if (solutions.empty())
        return Hit::NO_HIT();
   
	double t = numeric_limits<double>::max();
	for (vector<double>::iterator it = solutions.begin(); it != solutions.end(); ++it)
    {
        // cout << *it << ' ';
        if (*it > 0)
		  t = min(t, *it);
    }
    // cout << endl;
	
	if (t < ALMOST_ZERO)
        return Hit::NO_HIT();
	
	const Vector p = ray.at(t) - position;
    const double mult = p.length() - (r * r) - (R * R);
    const Vector component = p * (mult * 4);
        
    Vector N(component);
  	N.y += 8 * R * R * p.y;
    
    return Hit(t, N.normalized(), this);
}
