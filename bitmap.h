#ifndef BITMAP_H
#define BITMAP_H

#include "triple.h"

class Bitmap {
	public:
		virtual ~Bitmap() {};
		virtual const Color &colorAt(double u, double v) const = 0;
};

#endif
