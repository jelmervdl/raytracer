/**
 * Raytracer Code for Computer Graphics course 2012
 * Sander Feringa (1606913) and Jelmer van der Linde (1772791)
 */
//
//  Framework for a raytracer
//  File: scene.cpp
//
//  Created for the Computer Science course "Introduction Computer Graphics"
//  taught at the University of Groningen by Tobias Isenberg.
//
//  Authors:
//    Maarten Everts
//    Jasper van de Gronde
//
//  This framework is inspired by and uses code of the raytracer framework of 
//  Bert Freudenberg that can be found at
//  http://isgwww.cs.uni-magdeburg.de/graphik/lehre/cg2/projekt/rtprojekt.html 
//

#include "scene.h"
#include "material.h"
#include <cassert>
#include <ctime>
#include <sstream>
#include <iomanip>

const double GOLDEN_ANGLE = 137.508 / 180 * M_PI;

using namespace std;

string time_since(time_t start)
{
    time_t now;
    time(&now);

    int seconds = difftime(now, start);

    stringstream out;
    out << setfill('0') << setw(2) << seconds / 60
        << ":"
        << setfill('0') << setw(2) << seconds % 60;
    return out.str();
}

Hit Scene::findHit(const Ray &ray)
{
    // Find hit object and distance
    Hit min_hit(numeric_limits<double>::infinity(),Vector(), NULL);

    for (size_t i = 0; i < objects.size(); ++i)
    {
        Hit hit(objects[i]->intersect(ray));

        if (hit.t < min_hit.t)
            min_hit = hit;
    }

    return min_hit;
}

void Scene::render(Image &img)
{
    switch (renderMode)
    {
        case PHONG:
            renderPixels(&Scene::tracePhong, img);
            break;

        case ZBUFFER:
            renderZBuffer(img);
            break;

        case NORMAL:
            renderPixels(&Scene::traceNormal, img);
            break;

        case GOOCH:
            renderPixels(&Scene::traceGooch, img);
            break;
    }
}

Color Scene::tracePhong(const Ray &ray)
{
    return tracePhongRecursive(ray, 0);
}

Color Scene::tracePhongRecursive(const Ray &ray, unsigned int depth)
{
    // Find the frontmost object that the ray hits.
    Hit min_hit = findHit(ray);

    // No hit? Return background color.
    if (!min_hit.object)
        return Color(0.0, 0.0, 0.0);

    Material *material = min_hit.object->material; //the hit objects material
    Object *object = min_hit.object;
    Point hit = ray.at(min_hit.t);                 //the hit point
    Vector N = min_hit.N;                          //the normal at hit point
    Vector V = -ray.D;                             //the view vector
    
    // Phong illumination
    Color color;

    // add illumination for each light.
    for (size_t i = 0; i < lights.size(); ++i)
    {
        // Ray direction from the light to the point of impact on the object
        Vector L = lights[i]->position - hit;
        L.normalize();
        
        // Ambient lighting
        color += lights[i]->color * object->colorAt(hit) * material->ka;

        // Shadows
        if (renderShadows)
        {
            // Light ray from the light source to the object hit
            Ray light_ray(lights[i]->position, (hit - lights[i]->position).normalized());
            Hit obstructing_hit = findHit(light_ray);

            // If the ray is blocked by another object, skip this light source.
            if (obstructing_hit.object != min_hit.object)
                continue;
        }

        // Angle between normal and incomming light ray (also, reflected ray!)
        double angle = L.dot(N);

        // Diffuse lighting, only when coming from the front.
        if (angle > 0)
            color += lights[i]->color * object->colorAt(hit) * material->kd * angle;
        
        // Reflected ray direction
        Vector R = -L.mirror(N);

        // Angle between the view and reflected ray vectors.
        double reflection_angle = R.dot(V);

        // Specular lighting, only if we are not shining on it from the 'back'.
        if (reflection_angle > 0)
            color += lights[i]->color * material->ks * pow(reflection_angle, material->n);
    }

    // Add reflection
    if (depth < maxRecursionDepth)
    {
        // Reflected view ray direction, mirrored in the normal.
        Vector RV = ray.D.mirror(N).normalized();
        
        // Add the light reflected by other objects to the light for this point.
        color += tracePhongRecursive(Ray(hit, RV), depth + 1) * material->ks;
    }

    return color;
}

Color Scene::traceNormal(const Ray &ray)
{
    // Find the frontmost object that the ray hits.
    Hit min_hit = findHit(ray);

    // No hit? Return background color.
    if (!min_hit.object)
        return Color(0.0, 0.0, 0.0);

    Point hit = ray.at(min_hit.t);
    Vector N = min_hit.N; // normal of the hit point
    
    // Push the <-1,1> range of vectors into <0,1> range of colors.
    return (N + 1) / 2.0;
}

void Scene::renderZBuffer(Image &img)
{
    /*
    int w = img.width();
    int h = img.height();

    double min_t = std::numeric_limits<double>::infinity();
    double max_t = 0.0;

    // Buffer for the found t-values.
    double *z_values = new double[w * h];

    // Find minimum and maximum t values.
    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            Point pixel(x+0.5, h-1-y+0.5, 0);
            Ray ray(eye, (pixel-eye).normalized());
            Hit hit = findHit(ray);

            z_values[y * w + x] = hit.t;

            if (hit.object == NULL)
                continue;

            if (hit.t < min_t)
                min_t = hit.t;

            if (hit.t > max_t)
                max_t = hit.t;
        }
    }

    // Add a bit of space between the furthest object and the background.
    // Fugly hack to make the object that is most in the back also visible. 
    max_t += 0.25 * (max_t - min_t);

    // Render
    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            double t = z_values[y * w + x];

            if (t < std::numeric_limits<double>::infinity()) {
                double z = 1.0 - (t - min_t) / (max_t - min_t);
                img(x,y) = Color(z, z, z);
            }
            else
                img(x,y) = Color(0, 0, 0);
        }
    }

    delete[] z_values;
    */
}

Color Scene::traceGooch(const Ray &ray)
{
    Hit hit = findHit(ray);
    Vector N = hit.N;
    Vector V = -ray.D;

    Color color;

    for (size_t i = 0; i < lights.size(); ++i)
    {
        Vector L = lights[i]->position - ray.at(hit.t);
        L.normalize();

        double angle = L.dot(N);

        Color kd = lights[i]->color * hit.object->material->color * hit.object->material->kd;

        Color kCool = Color(0, 0, goochParameters.b) + goochParameters.alpha * kd;
        Color kWarm = Color(goochParameters.y, goochParameters.y, 0) + goochParameters.beta * kd;

        // Gooch (also: diffuse)
        color += kCool * ((1.0 - angle) / 2) + kWarm * ((1.0 + angle) / 2);

        // Reflected ray direction
        Vector R = -L.mirror(N);

        // Angle between the view and reflected ray vectors.
        double reflection_angle = R.dot(V);

        // Specular lighting, only if we are not shining on it from the 'back'.
        if (reflection_angle > 0)
            color += lights[i]->color * hit.object->material->ks * pow(reflection_angle, hit.object->material->n);
    }

    return color;
}

void Scene::renderPixels(traceFunctionPtr traceFunction, Image &img)
{
    assert(img.width() == camera.width && img.height() == camera.height);

    int w = img.width();
    int h = img.height();
    
    // C for Fermat's spiral, later on used in blurring.
    const double c = camera.apertureRadius / (camera.up.length() * sqrt(camera.apertureSamples));

    // For each pixel...
    size_t progress = 0;
    time_t start;
    time(&start); 

    #pragma omp parallel for
    for (int y = 0; y < h; ++y)
    {
        time_t row_start;
        time(&row_start);

        for (int x = 0; x < w; ++x)
        {
            Color col(0.0, 0.0, 0.0);

            // Trace multiple rays for each pixel depending on how much supersampling we want.
            for (unsigned int x_sample = 1; x_sample <= superSamplingFactor; ++x_sample)
            {
                for (unsigned int y_sample = 1; y_sample <= superSamplingFactor; ++y_sample)
                {
                    // Super-sampling in-the-margin pixel offset.
                    Point pixel(
                        x + x_sample / (superSamplingFactor + 1.0),
                        y + y_sample / (superSamplingFactor + 1.0),
                        0);
                    
                    // The point the camera looks towards, corrected with the up-vector.
                    Point p = camera.center
                        + (pixel.x - .5 * camera.width)  * camera.right()
                        + (pixel.y - .5 * camera.height) * -camera.up;

                    // And take the average of a few samples to induce blurring in the front using Vogel's model.
                    for (unsigned int apertureSample = 0; apertureSample < camera.apertureSamples; ++apertureSample)
                    {
                        double r = c * sqrt(apertureSample);
                        double th = apertureSample * GOLDEN_ANGLE;

                        // 'shake!'
                        Point apertureOffset(r * cos(th), r * sin(th), 0);

                        Ray ray = Ray(camera.eye + apertureOffset, (p - camera.eye - apertureOffset).normalized());

                        // Trace the ray
                        col += (this->*traceFunction)(ray);
                    }
                }
            }
            
            // Average the color of all the subsamplings.
            col = col / (superSamplingFactor * superSamplingFactor) / camera.apertureSamples;

            col.clamp();
            img(x,y) = col;
        }

        #pragma omp critical(report_progress)
        cout << ++progress << " of " << h << '\t' << time_since(row_start) << " (" << time_since(start) << ')' << endl;
    }
}

void Scene::addObject(Object *o)
{
    objects.push_back(o);
}

void Scene::addLight(Light *l)
{
    lights.push_back(l);
}

void Scene::setEye(Triple e)
{
    camera.eye = e;

    // Fallback values to simulate old Eye-only implementation.
    camera.center = Triple(e.x, e.y, 0);
    camera.up = Triple(0, 1, 0);
    camera.width = 400;
    camera.height = 400;
}

void Scene::setCamera(const Camera &c)
{
    camera = c;
}

void Scene::setRenderMode(RenderMode mode)
{
    renderMode = mode;
}

void Scene::setGoochParameters(GoochParameters const &parameters)
{
    goochParameters = parameters;
}

void Scene::setRenderShadows(bool render_shadows)
{
    renderShadows = render_shadows;
}

void Scene::setMaxRecursionDepth(unsigned int depth)
{
    maxRecursionDepth = depth;
}

void Scene::setSuperSamplingFactor(unsigned int factor)
{
    superSamplingFactor = factor;
}