/**
 * Raytracer Code for Computer Graphics course 2012
 * Sander Feringa (1606913) and Jelmer van der Linde (1772791)
 */

#ifndef PLANNE_H
#define PLANNE_H

#include "object.h"

class Plane : public Object
{
public:
    Plane(Vector normal, double distance)
    : N(normal), d(distance) { }

    virtual Hit intersect(const Ray &ray);

    const Vector N;
    const double d;
};

#endif
