/**
 * Raytracer Code for Computer Graphics course 2012
 * Sander Feringa (1606913) and Jelmer van der Linde (1772791)
 */

#ifndef CYLINDER_H
#define CYLINDER_H

#include "object.h"

class Cylinder : public Object
{
public:
    Cylinder(Point p1, Point p2, double r)
    : P1(p1), P2(p2), r(r) { }

    virtual Hit intersect(const Ray &ray);

    const Point P1;
    const Point P2;
    const double r;
};

#endif
