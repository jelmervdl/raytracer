/**
 * Raytracer Code for Computer Graphics course 2012
 * Sander Feringa (1606913) and Jelmer van der Linde (1772791)
 */
//
//  Framework for a raytracer
//  File: material.h
//
//  Created for the Computer Science course "Introduction Computer Graphics"
//  taught at the University of Groningen by Tobias Isenberg.
//
//  Author: Maarten Everts
//
//  This framework is inspired by and uses code of the raytracer framework of 
//  Bert Freudenberg that can be found at
//  http://isgwww.cs.uni-magdeburg.de/graphik/lehre/cg2/projekt/rtprojekt.html 
//

#ifndef MATERIAL_H
#define MATERIAL_H

#include "triple.h"
#include "bitmap.h"

class Material
{
public:
    Color color;        // base color
    double ka;          // ambient intensity
    double kd;          // diffuse intensity
    double ks;          // specular intensity 
    double n;           // exponent for specular highlight size

    Bitmap *texture;

    Material();
    ~Material();

    void loadTexture(std::string const &filename);

    friend ostream& operator<<(ostream &out, Material const &m);
};

#endif
