/**
 * Raytracer Code for Computer Graphics course 2012
 * Sander Feringa (1606913) and Jelmer van der Linde (1772791)
 */

#include "triangle.h"
#include <algorithm>
#include <iostream>
#include <math.h>

static const double ALMOST_ZERO = 1.0e-10; // Floating points arithmetic isn't science, it is stupid.

Triangle::Triangle(Point V0, Point V1, Point V2)
:
    V0(V0),
    V1(V1),
    V2(V2)
{
    Vector U = V1 - V0;
    Vector V = V2 - V0;
    Vector N = U.cross(V);
    N.normalize();

    N0 = N;
    N1 = N;
    N2 = N;

    precompute();
}

Triangle::Triangle(Point V0, Point V1, Point V2, Vector N0, Vector N1, Vector N2)
:
    V0(V0),
    V1(V1),
    V2(V2),
    N0(N0.normalized()),
    N1(N1.normalized()),
    N2(N2.normalized())
{
    precompute();
}

void Triangle::precompute()
{
    U = V1 - V0;
    V = V2 - V0;
    N = U.cross(V);

    uv = U.dot(V);
    uu = U.dot(U);
    vv = V.dot(V);

    dem = (uv * uv - uu * vv);
}

Hit Triangle::intersect(const Ray &ray)
{
    // Based on http://softsurfer.com/Archive/algorithm_0105/algorithm_0105.htm
    
    // Point on the ray that intersects with the triangle
    double r = -(N.dot(ray.O - V0)) / N.dot(ray.D);
    
    // The intersection is behind the light source :/
    if (r < ALMOST_ZERO)
    	return Hit::NO_HIT();

    // Point of intersection with the triangle
    Vector W = ray.at(r) - V0; // relative point of intersection :)
	
	double wv = W.dot(V);
	double wu = W.dot(U);
	
    double si  = (uv * wv - vv * wu) / dem;
    double ti  = (uv * wu - uu * wv) / dem;
	
	// Does the intersection with the plane happen between the bounds
	// of our triangle? If not, no candy.
	if (si < ALMOST_ZERO || ti < ALMOST_ZERO || si + ti > 1)
		return Hit::NO_HIT();
	
    // Interpolate the normal using the vertex normals
	return Hit(r, (1.0 - (si + ti)) * N0 + si * N1 + ti * N2, this);
}