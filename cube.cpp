/**
 * Raytracer Code for Computer Graphics course 2012
 * Sander Feringa (1606913) and Jelmer van der Linde (1772791)
 */

#include <cassert>
#include <iostream>

#include "cube.h"
#include "matrix.h"

Cube::Cube(Point const &position, Vector const &size, Vector const &axis, double angle)
:
    position(position),
    size(size),
    axis(axis),
    angle(angle)
{
    /*
      3 ----- 4
     /|      /|
    1 ----- 2 |
    | 8 ----| 7
    |/      |/
    5 ----- 6
    */

    // Haha, this is no cube, this is just a tribute.
    // Made by Triangles Inc.

    Matrix r(axis, (angle / 180) * M_PI);
    
    Point p1 = position + r * (Point(0, 1, 1) * size);
    Point p2 = position + r * (Point(1, 1, 1) * size);
    Point p3 = position + r * (Point(0, 1, 0) * size);
    Point p4 = position + r * (Point(1, 1, 0) * size);

    Point p5 = position + r * (Point(0, 0, 1) * size);
    Point p6 = position + r * (Point(1, 0, 1) * size);
    Point p7 = position + r * (Point(1, 0, 0) * size);
    Point p8 = position + r * (Point(0, 0, 0) * size);

    Vector n = r * Vector(0, 0, -1);
    Vector e = r * Vector(1, 0, 0);
    Vector s = r * Vector(0, 0, 1);
    Vector w = r * Vector(-1, 0, 0);
    Vector u = r * Vector(0, 1, 0);
    Vector d = r * Vector(0, -1, 0);

    // front
    triangles[0] = new Triangle(p2, p5, p1, s, s, s);
    triangles[1] = new Triangle(p6, p2, p5, s, s, s);

    // right
    triangles[2] = new Triangle(p6, p7, p2, e, e, e);
    triangles[3] = new Triangle(p7, p4, p2, e, e, e);

    // back
    triangles[4] = new Triangle(p8, p7, p3, n, n, n);
    triangles[5] = new Triangle(p7, p4, p3, n, n, n);

    // left
    triangles[6] = new Triangle(p5, p8, p1, w, w, w);
    triangles[7] = new Triangle(p8, p3, p1, w, w, w);

    // top
    triangles[8] = new Triangle(p1, p2, p3, u, u, u);
    triangles[9] = new Triangle(p2, p4, p3, u, u, u);

    // bottom
    triangles[10] = new Triangle(p5, p6, p8, d, d, d);
    triangles[11] = new Triangle(p6, p7, p8, d, d, d);
}

Cube::Cube(Cube const &other)
:
    position(other.position),
    size(other.size),
    axis(other.axis),
    angle(other.angle)
{
    for (size_t i = 0; i < 12; ++i)
        triangles[i] = new Triangle(*other.triangles[i]);
}

Cube::~Cube()
{
    for (size_t i = 0; i < 12; ++i)
        delete triangles[i];
}

Hit Cube::intersect(Ray const &ray)
{
    Hit min_hit = triangles[0]->intersect(ray);

    for (size_t i = 1; i < 12; ++i)
    {
        Hit hit(triangles[i]->intersect(ray));

        if (!min_hit.object || hit.t < min_hit.t)
            min_hit = hit;
    }

    return Hit(min_hit.t, min_hit.N, min_hit.object ? this : 0);
}

Color Cube::colorAt(Point const &hit)
{
    return material->color;
}
