/**
 * Raytracer Code for Computer Graphics course 2012
 * Sander Feringa (1606913) and Jelmer van der Linde (1772791)
 */
//
//  Framework for a raytracer
//  File: sphere.cpp
//
//  Created for the Computer Science course "Introduction Computer Graphics"
//  taught at the University of Groningen by Tobias Isenberg.
//
//  Authors:
//    Maarten Everts
//    Jasper van de Gronde
//
//  This framework is inspired by and uses code of the raytracer framework of 
//  Bert Freudenberg that can be found at
//  http://isgwww.cs.uni-magdeburg.de/graphik/lehre/cg2/projekt/rtprojekt.html 
//

#include "sphere.h"
#include <algorithm>
#include <iostream>
#include <math.h>
#include <limits>
#include <cassert>

// Floating points arithmetic isn't science, it is stupid.
static const double ALMOST_ZERO = 1.0e-10;

Sphere::Sphere(Point position, double r)
:
    position(position),
    r(r)
{}

Sphere::Sphere(Point position, double r, Vector axis, double angle)
:
    position(position),
    r(r),
    rotation(axis.normalized(), angle / 180 * M_PI)
{}

Hit Sphere::intersect(const Ray &ray)
{
    /****************************************************
    * RT1.1: INTERSECTION CALCULATION
    *
    * Given: ray, position, r
    * Sought: intersects? if true: *t
    * 
    * Insert calculation of ray/sphere intersection here. 
    *
    * You have the sphere's center (C) and radius (r) as well as
    * the ray's origin (ray.O) and direction (ray.D).
    *
    * If the ray does not intersect the sphere, return false.
    * Otherwise, return true and place the distance of the
    * intersection point from the ray origin in *t (see example).
    ****************************************************/

    // taken from http://wiki.cgsociety.org/index.php/Ray_Sphere_Intersection
    
    // Translate the ray's position to object space
    Point local_ray_pos = ray.O - position;
    // TODO should we do something alike for the ray's direction?

    double a = ray.D.dot(ray.D);
    double b = 2.0 * ray.D.dot(local_ray_pos);
    double c = local_ray_pos.dot(local_ray_pos) - r * r;

    double discriminant = b * b - 4.0 * a * c;

    // If the discriminant is negative, there are no real roots;
    // the ray never touches nor intersects the sphere.
    if (discriminant < ALMOST_ZERO)
        return Hit::NO_HIT();
    
    // compute q
    double disc_sqrt = sqrt(discriminant);
    double q = b < ALMOST_ZERO
        ? (-b - disc_sqrt) / 2.0
        : (-b + disc_sqrt) / 2.0;
    
    // compute t0 and t1
    double t0 = q / a;
    double t1 = c / q;
    
    // prefer a t that is not negative, because that would be odd,
    // a ray that goes in the opposite direction.
    double t = t0 < ALMOST_ZERO ? t1 : t0;
    
    // if both t's are negative, the object is on the other side
    // of the ray apparently. No hits.
    if (t < ALMOST_ZERO)
        return Hit::NO_HIT();

    // Calculate normal.
    Vector N = ray.at(t) - position;

    return Hit(t,N.normalized(), this);
}

Color Sphere::colorAt(Point const &original_hit)
{
    if (!material->texture)
        return material->color;

    // todo: rotate "point" d using the axis vector
    Vector d = rotation * (original_hit - position);
    //Vector d = (original_hit - position).rotate(up, -angle);

    double theta = acos(d.z / r);
    double phi = atan2(d.y, d.x);

    // Subtract the angle which rotates around the z-axis
    // phi -= angle;

    // Make phi positive (I don't trust people who use enormous angles)
    while (phi < 0)
        phi += 2 * M_PI;
    
    assert(phi <= 2 * M_PI);

    double u = phi / (2 * M_PI);
    double v = theta / M_PI;

    assert(u <= 1.0 && v <= 1.0);

    return material->texture->colorAt(u, v);
}
