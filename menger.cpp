/**
 * Raytracer Code for Computer Graphics course 2012
 * Sander Feringa (1606913) and Jelmer van der Linde (1772791)
 */

#include <cassert>
#include <iostream>

#include "menger.h"
#include "matrix.h"

Menger::Menger(Point const &position, Vector const &size, int depth, Vector const &axis, double angle)
:
    depth(depth),
    bounds(new Cube(position, size, axis, angle))
{
    Vector part_size = bounds->size / 3;
    size_t i = 0;

    Matrix r(axis, (angle / 180) * M_PI);

    for (int x = 0; x < 3; ++x)
        for (int y = 0; y < 3; ++y)
            for (int z = 0; z < 3; ++z)
                if (!(x == 1 && y == 1 || x == 1 && z == 1 || y == 1 && z == 1))
                    if (depth > 0)
                        parts[i++] = new Menger(position + r * Triple(x, y, z) * part_size, part_size, depth - 1, axis, angle);
                    else
                        parts[i++] = new Cube(position + r * Triple(x, y, z) * part_size, part_size, axis, angle);

    assert(i == 20);
}

Menger::~Menger()
{
    for (size_t i = 0; i < 20; ++i)
        delete parts[i];

    delete bounds;
}

Hit Menger::intersect(Ray const &ray)
{
    Hit hit = bounds->intersect(ray);

    if (!hit.isHit())
        return Hit::NO_HIT();

    Hit min_hit = parts[0]->intersect(ray);

    for (size_t i = 1; i < 20; ++i)
    {
        Hit hit(parts[i]->intersect(ray));

        if (!min_hit.isHit() || hit.t < min_hit.t)
            min_hit = hit;
    }

    if (!min_hit.isHit())
        return Hit::NO_HIT();

    return Hit(min_hit.t, min_hit.N, this);
}