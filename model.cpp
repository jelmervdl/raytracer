#include "model.h"
#include "glm.h"
#include "cube.h"
#include "matrix.h"
#include <cmath>

using namespace std;

Triple min(Triple const &a, Triple const &b)
{
    return Triple(
        min(a.x, b.x),
        min(a.y, b.y),
        min(a.z, b.z));
}

Triple max(Triple const &a, Triple const &b)
{
    return Triple(
        max(a.x, b.x),
        max(a.y, b.y),
        max(a.z, b.z));
}

Vector convertVector(GLfloat const *vector)
{
    return Vector(vector[0], vector[1], vector[2]);
}

double avg(GLfloat const *vector)
{
    return (vector[0] + vector[1] + vector[2]) / 3;
}

Material *convertMaterial(GLMmaterial const &material)
{
    Material *m = new Material;

    // Use the diffuse color as base color.
    m->color = Color(material.diffuse[0], material.diffuse[1], material.diffuse[2]);
    m->kd = avg(material.diffuse);

    // Derive the ambiant factor from how the ambient material relates to the diffuse color in the original model.
    m->ka = avg(material.ambient);

    // Derive the specular factor from the original material. Just take the brightest color, and use that.
    m->ks = avg(material.specular);

    // Luckily, shininess can be mapped 1:1.
    m->n = material.shininess;

    return m;
}

Model::Model(string const &filename, Point const &position, Vector const &axis, double angle, double scale, Material *material)
:
    position(position),
    scale(scale)
{
    this->material = material;

    GLMmodel *model = glmReadOBJ(const_cast<char*>(filename.c_str()));

    glmUnitize(model);

    // glmScale(model, 2.0);
    
    glmFacetNormals(model);
    glmVertexNormals(model, 90);

    // Maybe this is a bit optimistic, some groups are empty.
    // But a few pointers too many won't hurt memory.
    groups = new Group*[model->numgroups];
    
    // num_groups will be incremented each time a complete group is added.
    num_groups = 0;

    Matrix r(axis, angle / 180 * M_PI);

    GLMgroup *model_group = model->groups;
    while (model_group)
    {
        // Skip group if it is empty
        if (model_group->numtriangles == 0)
        {
            model_group = model_group->next;
            continue;
        }

        Group *group = new Group(model_group->numtriangles);

        // If a specific material was specified for this model, use that. Otherwise
        // create the material for all the triangles using the groups' material.
        if (this->material != 0)
            group->material = this->material;
        else
            group->material = convertMaterial(model->materials[model_group->material]);
        
        for (size_t i = 0; i < model_group->numtriangles; ++i)
        {
            GLMtriangle *triangle = &model->triangles[model_group->triangles[i]];

            group->triangles[i] = new Triangle(
                position + r * (convertVector(&model->vertices[3*triangle->vindices[0]]) * scale),
                position + r * (convertVector(&model->vertices[3*triangle->vindices[1]]) * scale),
                position + r * (convertVector(&model->vertices[3*triangle->vindices[2]]) * scale),
                (r * convertVector(&model->normals[3*triangle->nindices[0]])).normalized(),
                (r * convertVector(&model->normals[3*triangle->nindices[1]])).normalized(),
                (r * convertVector(&model->normals[3*triangle->nindices[2]])).normalized());
        }

        // Now we know which triangles are part of the group, generate the bounding sphere
        group->updateBoundingBox();

        // .. and finally, add the group to the list and go to the next group.
        groups[num_groups++] = group;
        model_group = model_group->next;
    }

    // The model has been converted, no need to keep it around.
    glmDelete(model);
}

Model::~Model()
{
    for (size_t i = 0; i < num_groups; ++i)
        delete groups[i];

    delete[] groups;
}

Hit Model::intersect(Ray const &ray)
{
    assert (num_groups > 0);

    Hit min_hit = groups[0]->intersect(ray);

    for (size_t i = 1; i < num_groups; ++i)
    {
        Hit hit(groups[i]->intersect(ray));

        if (!min_hit.object || hit.t < min_hit.t)
            min_hit = hit;
    }

    return min_hit;
}

Model::Group::Group(size_t num_triangles)
:
    boundingBox(0),
    triangles(new Triangle*[num_triangles]),
    num_triangles(num_triangles)
{}


Model::Group::~Group()
{
    for (size_t i = 0; i < num_triangles; ++i)
        delete triangles[i];

    delete[] triangles;
}

Hit Model::Group::intersect(Ray const &ray)
{
    assert(boundingBox != 0);

    // Ghehe, fun, show the bounding spheres ;)
    // return boundingBox->intersect(ray);

    // If the ray doesn't even hit the bounding sphere, don't bother.
    if (!boundingBox->intersect(ray).isHit())
        return Hit::NO_HIT();

    // Find out which (if any) triangle was hit by the ray.
    assert(num_triangles > 0);

    Hit min_hit = triangles[0]->intersect(ray);

    for (size_t i = 1; i < num_triangles; ++i)
    {
        Hit hit(triangles[i]->intersect(ray));

        if (!min_hit.isHit() || hit.t < min_hit.t)
            min_hit = hit;
    }

    // Return this, and not the triangle as the object hit
    // to make the model use our material instead of the material
    // of the triangle.
    return Hit(min_hit.t, min_hit.N, min_hit.object ? this : 0);
}

void Model::Group::updateBoundingBox()
{
    Point
        maximum(-numeric_limits<double>::infinity(), -numeric_limits<double>::infinity(), -numeric_limits<double>::infinity()),
        minimum(numeric_limits<double>::infinity(), numeric_limits<double>::infinity(), numeric_limits<double>::infinity());

    for (size_t i = 0; i < num_triangles; ++i)
    {
        Triangle const &triangle = *triangles[i];
        
        minimum = min(minimum, min(triangle.V0, min(triangle.V1, triangle.V2)));

        maximum = max(maximum, max(triangle.V0, max(triangle.V1, triangle.V2)));
    }

    if (boundingBox)
        delete boundingBox;

    boundingBox = new Cube(minimum, maximum - minimum);
    boundingBox->material = material;
}