/**
 * Raytracer Code for Computer Graphics course 2012
 * Sander Feringa (1606913) and Jelmer van der Linde (1772791)
 */
//
//  Framework for a raytracer
//  File: scene.h
//
//  Created for the Computer Science course "Introduction Computer Graphics"
//  taught at the University of Groningen by Tobias Isenberg.
//
//  Authors:
//    Maarten Everts
//    Jasper van de Gronde
//
//  This framework is inspired by and uses code of the raytracer framework of 
//  Bert Freudenberg that can be found at
//  http://isgwww.cs.uni-magdeburg.de/graphik/lehre/cg2/projekt/rtprojekt.html 
//

#ifndef SCENE_H_KNBLQLP6
#define SCENE_H_KNBLQLP6

#include <vector>
#include "triple.h"
#include "light.h"
#include "object.h"
#include "image.h"
#include "camera.h"

class Scene
{
public:
    enum RenderMode {
        PHONG,
        ZBUFFER,
        NORMAL,
        GOOCH
    };

    struct GoochParameters {
        double b;
        double y;
        double alpha;
        double beta;
    };

    typedef Color (Scene::*traceFunctionPtr)(Ray const &ray);

private:
    std::vector<Object*> objects;
    std::vector<Light*> lights;
    Camera camera;
    RenderMode renderMode;
    GoochParameters goochParameters;
    bool renderShadows;
    unsigned int maxRecursionDepth;
    unsigned int superSamplingFactor;

public:
    Scene() :
        renderMode(PHONG),
        renderShadows(false),
        maxRecursionDepth(1),
        superSamplingFactor(1)
        {};
    Color trace(const Ray &ray);
    void render(Image &img);
    void addObject(Object *o);
    void addLight(Light *l);
    void setEye(Triple e);
    void setCamera(const Camera &c);
    void setRenderMode(RenderMode mode);
    void setGoochParameters(GoochParameters const &parameters);
    void setRenderShadows(bool render_shadows);
    void setMaxRecursionDepth(unsigned int maxRecursionDepth);
    void setSuperSamplingFactor(unsigned int factor);
    unsigned int getNumObjects() { return objects.size(); }
    unsigned int getNumLights() { return lights.size(); }
    const Camera &getCamera() { return camera; }

private:
    Hit findHit(const Ray &ray);
    void renderPixels(traceFunctionPtr traceFunction, Image &img);
    
    void renderZBuffer(Image &img);

    Color tracePhong(const Ray &ray);
    Color tracePhongRecursive(const Ray &ray, unsigned int depth);

    Color traceNormal(const Ray &ray);

    Color traceGooch(const Ray &ray);
};

#endif /* end of include guard: SCENE_H_KNBLQLP6 */
