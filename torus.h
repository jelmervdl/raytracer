/**
 * Raytracer Code for Computer Graphics course 2012
 * Sander Feringa (1606913) and Jelmer van der Linde (1772791)
 */
 
#ifndef TORUS_H
#define TORUS_H

#include "object.h"

class Torus : public Object
{
public:
    Torus(Point position, double r, double R);

    virtual Hit intersect(const Ray &ray);

    //virtual Color colorAt(const Point &hit);

    const Point position;
    const double r;
    const double R;
};

#endif /* end of include guard: TORUS_H */