/**
 * Raytracer Code for Computer Graphics course 2012
 * Sander Feringa (1606913) and Jelmer van der Linde (1772791)
 */

#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "object.h"

class Triangle : public Object
{
public:
    Triangle(Point V0, Point V1, Point V2);
    Triangle(Point V0, Point V1, Point V2, Vector N0, Vector N1, Vector N2);

    virtual Hit intersect(const Ray &ray);

    const Point V0, V1, V2;
    
    Vector N0, N1, N2;

private:
    Vector U, V, N;
    double uv, uu, vv, dem;

    void precompute();
};

#endif