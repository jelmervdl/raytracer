/**
 * Raytracer Code for Computer Graphics course 2012
 * Sander Feringa (1606913) and Jelmer van der Linde (1772791)
 */

#ifndef CUBE_H
#define CUBE_H

#include "object.h"
#include "triangle.h"

class Cube : public Object
{
private:
	Triangle *triangles[12];

public:
	Point position;
	Vector size;

	Vector axis;
	double angle;

	Cube(Point const &position, Vector const &size, Vector const &axis = Vector(0,1,0), double angle = 0);
	Cube(Cube const &other);
	~Cube();

	virtual Hit intersect(const Ray &ray);

	virtual Color colorAt(const Point &hit);
};

#endif
