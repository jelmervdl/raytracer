############################################################################
# Makefile for the raytracer framwork created for the Computer Science 
# course "Introduction Computer Graphics" taught at the University of 
# Groningen by Tobias Isenberg.
############################################################################

### MACROS

# GNU (everywhere)
CPP = g++ -g -Wall -fopenmp

# GNU (faster) but for some reason it breaks with OpenMP.
#CPP = g++ -O5 -Wall -fomit-frame-pointer -ffast-math -fopenmp

LIBS = -lm

EXECUTABLE = ray

OBJS = main.o raytracer.o sphere.o plane.o triangle.o cylinder.o cube.o menger.o light.o material.o image.o triple.o lodepng.o scene.o model.o glm.o matrix.o torus.o

YAMLOBJS = $(subst .cpp,.o,$(wildcard yaml/*.cpp))

IMAGES = $(subst .yaml,.png,$(wildcard *.yaml))


### TARGETS

$(EXECUTABLE): $(OBJS) $(YAMLOBJS)
	$(CPP) $(OBJS) $(YAMLOBJS) $(LIBS) -o $@

run: $(IMAGES)

test: test.o triple.o
	$(CPP) test.o triple.o $(LIBS) -o test
	./test

%.png: %.yaml $(EXECUTABLE)
	./$(EXECUTABLE) $<

depend: make.dep

clean:
	- /bin/rm -f  *.bak *~ $(OBJS) $(YAMLOBJS) $(EXECUTABLE) $(EXECUTABLE).exe

make.dep:
	gcc -MM $(OBJS:.o=.cpp) > make.dep

### RULES

.SUFFIXES: .cpp .o .yaml .png

.cpp.o:
	$(CPP) -c -o $@ $<

### DEPENDENCIES

include make.dep
