#include "matrix.h"

Matrix::Matrix()
:
	r0(1, 0, 0),
	r1(0, 1, 0),
	r2(0, 0, 1)
{}

Matrix::Matrix(Vector const &r0, Vector const &r1, Vector const &r2)
:
	r0(r0),
	r1(r1),
	r2(r2)
{}

Matrix::Matrix(Vector const &axis, double angle)
{
	// Source: https://secure.wikimedia.org/wikipedia/en/wiki/Rotation_matrix#Rotation_matrix_from_axis_and_angle

	Vector u = axis.normalized();
	double ct = cos(angle);
	double st = sin(angle);

	r0 = Vector(
		ct + u.x * u.x * (1 - ct),
		u.x * u.y * (1 - ct) - u.z * st,
		u.x * u.z * (1 - ct) + u.y * st);

	r1 = Vector(
		u.y * u.x * (1 - ct) + u.z * st,
		ct + u.y * u.y * (1 - ct),
		u.y * u.z * (1 - ct) - u.x * st);

	r2 = Vector(
		u.z * u.x * (1 - ct) - u.y * st,
		u.z * u.y * (1 - ct) + u.x * st,
		ct + u.z * u.z * (1 - ct));
}

Vector Matrix::operator*(Vector const &f) const
{
	return Vector(
		r0.dot(f),
		r1.dot(f),
		r2.dot(f));
}

Matrix Matrix::operator*(Matrix const &m) const
{
	Matrix t = m.transposed();
	return Matrix(
		t * r0,
		t * r1,
		t * r2);
}

Matrix Matrix::transposed() const
{
	return Matrix(
		Vector(r0.x, r1.x, r2.x),
		Vector(r0.y, r1.y, r2.y),
		Vector(r0.z, r1.z, r2.z));
}