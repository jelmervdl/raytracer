/**
 * Raytracer Code for Computer Graphics course 2012
 * Sander Feringa (1606913) and Jelmer van der Linde (1772791)
 */

#ifndef MENGER_H
#define MENGER_H

#include "object.h"
#include "cube.h"

class Menger : public Object
{
private:
	int depth;
	Cube *bounds;
	Object *parts[20];

public:
	Menger(Point const &position, Vector const &size, int depth, Vector const &axis = Vector(0,1,0), double angle = 0);
	~Menger();

	virtual Hit intersect(const Ray &ray);
};

#endif