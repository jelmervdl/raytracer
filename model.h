#ifndef MODEL_H
#define MODEL_H

#include <string>

#include "object.h"
#include "triangle.h"

class Model : public Object
{
private:
    class Group : public Object
    {
    private:
        Object *boundingBox;
        Triangle **triangles;
        size_t num_triangles;
    public: 
        Group(size_t num_triangles);
        ~Group();
        virtual Hit intersect(const Ray &ray);

        void updateBoundingBox();

    friend class Model;
    };

    Group **groups;
    size_t num_groups;

    Point position;
    double scale;

public:
    Model(std::string const &filename, Point const &position, Vector const &axis, double angle, double scale, Material *material);
    ~Model();

    virtual Hit intersect(const Ray &ray);

    // virtual Color colorAt(const Point &hit);
};

#endif